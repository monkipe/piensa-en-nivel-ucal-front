function scrollToTop(scrollDuration) {
    var cosParameter = window.scrollY / 2,
        scrollCount = 0,
        oldTimestamp = performance.now();
    function step(newTimestamp) {
        scrollCount += Math.PI / (scrollDuration / (newTimestamp - oldTimestamp));
        if (scrollCount >= Math.PI) window.scrollTo(0, 0);
        if (window.scrollY === 0) return;
        window.scrollTo(0, Math.round(cosParameter + cosParameter * Math.cos(scrollCount)));
        oldTimestamp = newTimestamp;
        window.requestAnimationFrame(step);
    }
    window.requestAnimationFrame(step);
}

function showAniamtion() {
    const animationList = document.querySelectorAll(".Mnk-animation");
    for (const element of animationList) {
        if (!element.classList.contains('-js')) {
            element.classList.add("-active");
        }
    }
}

const elementFullpage = document.querySelector(".Mnk-portada");

window.addEventListener('resize', function (event) {
    if (elementFullpage) {
        if (window.innerWidth >= 1024) {
            firstSectionFullpage();
        } else {
            elementFullpage.style.height = "";
        }
    };
});

function firstSectionFullpage() {
    let
        heightElementOutSection = 0,
        heightCssElementFullpage;
    const heightElementOutSectionList = document.querySelectorAll(".Mnk-portada_section");
    elementFullpage.style.height = "";
    for (const element of heightElementOutSectionList) {
        heightElementOutSection = heightElementOutSection + element.outerHeight(true)
    }
    heightCssElementFullpage = window.innerHeight - heightElementOutSection;
    elementFullpage.style.height = heightCssElementFullpage + "px";
}

$(".Mnk-ancla_link").click(function (event) {
    event.preventDefault();
    $("html, body").animate({
        scrollTop: $($(this).attr("href")).offset().top - 55
    }, {
        duration: 500,
        easing: "swing"
    });
});

$(window).bind('scroll', function () {
    var navHeight = 60;
    if ($(window).scrollTop() > navHeight) {
        if (!$('.Mnk-header').hasClass('-no-clear-fixed')) {
            $('.Mnk-header').addClass('-fixed');
        };
    } else if ($(window).scrollTop() <= navHeight) {
        if (!$('.Mnk-header').hasClass('-no-clear-fixed')) {
            $('.Mnk-header').removeClass('-fixed');
        };
    };
});
