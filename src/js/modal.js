let elementModal;
const
    bodyElement = document.querySelector("body"),
    buttonModalList = document.querySelectorAll(".Mnk-modal_button_js"),
    buttonCloseModalList = document.querySelectorAll(".Mnk-modal_button-close");

for (const buttonModal of buttonModalList) {
    buttonModal.addEventListener('click', function (event) {
        let idModal = this.dataset.modal;
        elementModal = document.getElementById(idModal);
        if (!elementModal.classList.contains('-active')) {
            elementModal.classList.add("-active");
            bodyElement.classList.add("Mnk-modal_body");
        }
    })
}

for (const buttonCloseModal of buttonCloseModalList) {
    buttonCloseModal.addEventListener('click', function (event) {
        if (elementModal.classList.contains('-active')) {
            elementModal.classList.remove("-active");
            bodyElement.classList.remove("Mnk-modal_body");
        }
    })
}