window.onload = function () {

    // generic function to get URL parameter
    let getParameterURL = function (parameter) {
        let pageUrl = decodeURIComponent(window.location.search.substring(1)),
            parameters = pageUrl.split('&'),
            parameterName,
            i

        for (i = 0; i < parameters.length; i++) {
            parameterName = parameters[i].split('=')

            if (parameterName[0] === parameter) {
                return parameterName[1] === undefined ? true : parameterName[1]
            }
        }
    }

    let home = (function () {
        let init,
            load,
            printVideos,
            printDataVideo

        load = function () {
            // adding class to html tag
            $('html').addClass('-complete')

            // scroll to top of the page
            //scrollToTop(0)

            // showing elements
            showAniamtion()

            // validating full page element
            if (elementFullpage) {
                firstSectionFullpage()
            }
        }

        printDataVideo = function ($modal, index, dataVideo, typeImg) {
            $modal.find('.video_' + index + ' .img-video').attr('src', dataVideo[typeImg]).attr('data-id-video', dataVideo.id_video)
            $modal.find('.video_' + index + ' .title-video').html(dataVideo.title)
            $modal.find('.video_' + index).attr('data-index-video', dataVideo.index)
        }

        printVideos = function ($modal, indexVideo, dataVideos) {
            // reset modal
            $('.Mnk-modal .video-youtube').html('')
            $('.video_1 .Mnk-figure_img').removeClass('-ocult')

            // validating to print videos
            if (indexVideo && dataVideos.hasOwnProperty('video_' + indexVideo)) {
                let count = 2

                // printing principal video
                printDataVideo($modal, 1, dataVideos['video_' + indexVideo], 'cover')

                // printing others videos
                $.each(dataVideos, function (i, dataVideo) {
                    if (i !== 'video_' + indexVideo && i !== 'video_1' && dataVideo.active === 'Si') {
                        printDataVideo($modal, count, dataVideo, 'thumbnail')

                        count++
                    }
                })
            } else {
                // printing principal video
                printDataVideo($modal, 1, dataVideos.video_1, 'cover')

                // printing others videos
                printDataVideo($modal, 2, dataVideos.video_2, 'thumbnail')
                printDataVideo($modal, 3, dataVideos.video_3, 'thumbnail')
                printDataVideo($modal, 4, dataVideos.video_4, 'thumbnail')
            }
        }

        // Slider
        class Slider {
            constructor($slider) {
                this.$slider = $slider
                this.time = null
                this.sections = ['arquitectura', 'diseno-grafico', 'comunicacion', 'marketing']
                this.currentSection = ['', -1]
            }

            init() {
                this.load()
                this.mouseEvent()
                //this.goTo()
                this.afterChange()
                this.clickNav()
                this.swipe()
            }

            gotoTop() {
                $('html, body').animate({
                    scrollTop: $('#home').offset().top - 50
                }, 600)
            }

            goToForm() {
                $('html, body').animate({
                    scrollTop: $('#inscribete').offset().top - 50
                }, 600)
            }

            load() {
                this.$slider.slick({
                    infinite: false,
                    slidesToShow: 1,
                    swipe: false,
                    dots: true,
                    responsive: [
                        {
                          breakpoint: 769,
                          settings: {
                            swipe: true
                          }
                        }
                      ]
                })

                // adding negative class to active slide
                this.addNegativeClass()
            }

            mouseEvent() {
                let scrollCount = null,
                    scroll = null,
                    scrollFirstSlide = false,
                    scrollLasSlide = false,
                    stateFirstScrollSlick = null,
                    stateLastScrollSlick = null,
                    numSlides = $('#videos .Mnk-container').size() - 1

                this.$slider.on('wheel', (function (e) {
                    let slideIndex = this.$slider.slick('slickCurrentSlide')

                    // reset event scroll
                    e.preventDefault()

                    // reset and setting negative class
                    this.$slider.find('.slick-slide').removeClass('slick-negative')

                    // clearing scroll time out
                    clearTimeout(scroll)

                    // setting scroll time out
                    scroll = setTimeout(function () {
                        scrollCount = 0
                    }, 200)

                    // validating scroll count
                    if (scrollCount) {
                        return 0
                    }

                    // setting scroll count
                    scrollCount = 1

                    // validating deltaY scroll
                    if (e.originalEvent.deltaY < 0) {
                        this.$slider.slick('slickPrev')

                        if (slideIndex === 0 && scrollFirstSlide) {
                            this.gotoTop()
                            setTimeout(function () {
                                scrollFirstSlide = false
                            }, 500)
                        }

                        if (slideIndex === 0 && !scrollFirstSlide) {
                            scrollFirstSlide = true
                            stateFirstScrollSlick = true
                        }

                        if (stateLastScrollSlick && slideIndex !== numSlides) {
                            scrollLasSlide = false
                        }
                    } else {
                        this.$slider.slick('slickNext')

                        if (slideIndex === numSlides && scrollLasSlide) {
                            this.goToForm()
                            setTimeout(function () {
                                scrollLasSlide = false
                            }, 500)
                        }

                        if (slideIndex === numSlides && !scrollLasSlide) {
                            scrollLasSlide = true
                            stateLastScrollSlick = true
                        }

                        if (stateFirstScrollSlick && slideIndex !== 0) {
                            scrollFirstSlide = false
                        }
                    }

                    // adding negative class to active slide
                    this.addNegativeClass()
                }.bind(this)))
            }

            goTo(section) {
                let indexSection = this.sections.indexOf(section)

                // validating
                if (section && indexSection > -1) {
                    // refresh url
                    this.refreshUrl()

                    // scroll to
                    $('html,body').animate({
                        scrollTop: this.$slider.offset().top
                    }, 600)

                    // setting and go to slide
                    this.currentSection = [section, indexSection]
                    this.$slider.slick('slickGoTo', indexSection)

                    // adding negative class to active slide
                    this.addNegativeClass()
                } else {
                    this.currentSection = ['', -1]
                }
            }

            afterChange() {
                this.$slider.on('afterChange', function (event, slick, currentSlide) {
                    this.currentSection = [this.sections[currentSlide], currentSlide]

                    // refresh
                    this.refreshUrl()
                }.bind(this))
            }

            clickNav() {
                this.$slider.find('.slick-arrow, .slick-dots li').on('click', function () {
                    this.addNegativeClass()
                }.bind(this))
            }

            swipe() {
                this.$slider.on('swipe', function(){
                    this.addNegativeClass()
                }.bind(this))
            }

            refreshUrl() {
                window.history.pushState('Object', 'Title', '?retos=' + this.currentSection[0])
            }

            addNegativeClass() {
                // adding negative class to active slide

                setTimeout(function () {
                    this.$slider.find('.slick-slide').removeClass('slick-negative')
                    this.$slider.find('.slick-active').next('.slick-slide').addClass('slick-negative')
                }.bind(this), 100)

            }
        }

        class Modal {
            constructor() {
            }

            init() {
                this.open()
                this.close()
                this.clickThumnails()
            }

            open() {
                let dataVideos,
                    indexVideo,
                    $currentModal

                $('.Mnk-modal_button').on('click', function (e) {
                    indexVideo = $(this).attr('data-index-video')
                    $currentModal = $('#' + $(this).attr('data-modal'))
                    dataVideos = JSON.parse($currentModal.attr('data-json'))

                    // print videos
                    printVideos($currentModal, indexVideo, dataVideos)

                    // adding active class to open modal
                    $currentModal.addClass('-active')
                    $('body').addClass('Mnk-modal_body')
                })
            }

            close() {
                $('.Mnk-modal_button-close').on('click', function () {
                    // reset modal
                    $('.Mnk-modal .video-youtube').html('')
                    $('.video_1 .Mnk-figure_img').removeClass('-ocult')

                    $('.Mnk-modal').removeClass('-active')
                    $('body').removeClass('Mnk-modal_body')
                })
            }

            clickThumnails() {
                let dataVideos,
                    indexVideo,
                    $currentModal

                $('.Mnk-aside_video-modal .Mnk-figure').on('click', function () {
                    indexVideo = $(this).attr('data-index-video')
                    $currentModal = $('.Mnk-modal.-active')
                    dataVideos = JSON.parse($currentModal.attr('data-json'))

                    // print videos
                    printVideos($currentModal, indexVideo, dataVideos)
                })
            }
        }

        class Form {
            constructor() {
            }

            init() {
                this.validate()
                //this.datepicker()
                this.reset()
                this.mask()
                this.checkbox()
            }

            datepicker() {
                $('#graduate_year').datepicker({
                    format: 'dd/mm/yyyy',
                    endDate: 'today',
                    autoHide: true,
                    language: 'es-ES'
                }).on('change', function() {
                    $(this).valid()
                })
            }

            checkbox() {
                $('#terms').change(function() {
                    if(this.checked) {
                        $('#ucal_form .Mnk-button').removeClass('disabled')
                    } else {
                        $('#ucal_form .Mnk-button').addClass('disabled')
                    }
                });
            }

            mask () {
                $('#name, #last_name').mask('SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS', {
                    translation: {
                        'S': {
                            pattern: /[A-Za-z0-9 ]/,
                            optional: false
                        }
                    }
                })

                //$('#graduate_year').mask('00/00/0000')

                $('#phone').mask('Z00000000', {
                    translation: {
                        'Z': {
                            pattern: /[9]/,
                            optional: false
                        }
                    }
                })
            }

            reset() {
                $('#Mnk-thanks .Mnk-button').on('click', function() {
                    $('#ucal_form input, #ucal_form select').val('')
                    $('#ucal_form select').change()
                    $('#ucal_form #terms').prop("checked", false).change()

                    $('#ucal_form, #form-title').fadeIn()
                    $('#Mnk-thanks').removeClass('active')
                })
            }

            validate() {
                $('#ucal_form').on('submit', function (e) {
                    e.preventDefault()
                }.bind(this)).validate({
                    rules: {
                        name: {
                            required: true,
                            maxlength: 40
                        },
                        last_name: {
                            required: true,
                            maxlength: 40
                        },
                        graduate_year: {
                            required: true
                        },
                        phone: {
                            required: true,
                            maxlength: 9
                        },
                        email: {
                            required: true,
                            maxlength: 60,
                            validate_email: true
                        },
                        career: {
                            required: true,
                            maxlength: 40
                        }
                    },
                    messages: {
                        name: {
                            required: 'Requerido.',
                            maxlength: 'M&aacute;x. 40 caracteres'
                        },
                        last_name: {
                            required: 'Requerido.',
                            maxlength: 'M&aacute;x. 40 caracteres'
                        },
                        graduate_year: {
                            required: 'Requerido.'
                        },
                        phone: {
                            required: 'Requerido.',
                            maxlength: 'M&aacute;x. 9 caracteres'
                        },
                        email: {
                            required: 'Requerido.',
                            maxlength: 'M&aacute;x. 60 caracteres',
                            email: "Por favor ingrese un correo válido"
                        },
                        career: {
                            required: 'Requerido.',
                            maxlength: 'M&aacute;x. 40 caracteres'
                        }
                    },
                    submitHandler: function () {
                        $.ajax({
                            url: MNK_ADMIN_URL,
                            type: 'post',
                            dataType: 'json',
                            data: {
                                action: 'send_suscription_form',
                                dataString: $('#ucal_form').serialize()
                            },
                            success: function () {
                                fbq('track', 'CompleteRegistration')
                                $('#ucal_form, #form-title').fadeOut(200)
                                $('#Mnk-thanks').addClass('active')
                            }
                        });
                        return false;
                    }
                })

                $.validator.addMethod("validate_email", function (value, element) {
                    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
                        return true
                    }
                    else {
                        return false
                    }
                }, 'Correo Inv&aacute;lido.')
            }
        }

        init = function () {
            debugger
            let $slider = $('#videos'),
                section = getParameterURL('retos')

            // load page
            load()

            // load slider
            let slider = new Slider($slider)
            console.log(section)
            slider.init()
            slider.goTo(section)

            // event menu link
            $('.sub-menu .Mnk-ancla_link').on('click',function () {
                // setting and go to slide
                let section = $(this).attr('data-video')
                slider.goTo(section)
            })

            // load modal
            new Modal().init()

            // load video
            $('.video_1 .Mnk-figure_img').on('click', function () {
                $(this).addClass('-ocult')
                let videoId = $(this).find('.content_video img').attr('data-id-video')

                console.log(videoId)

                setTimeout(function () {
                    // load video
                    $('.Mnk-modal.-active .video-youtube').html('<iframe width="600" height="407" src="https://www.youtube.com/embed/' + videoId + '?rel=0&showinfo=0&autoplay=1" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" frameborder="0" allowfullscreen></iframe>')
                }, 50)
            })

            // load form
            new Form().init()
        }

        return {
            init: init
        }
    }())

    home.init()
}

