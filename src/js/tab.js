$('.Mnk-tab_boton').click(function(e){
    e.preventDefault();
    contenedor = $(this).data('contenedor');
    var id_tab = this.href.split("#")[1];
    id_parent = $(this).parents('.'+contenedor);
    if($(id_parent).find('.Mnk-tab_boton').filter('[data-contenedor="'+contenedor+'"]').hasClass('-active')){
        if(!$(this).hasClass('-active')){
            $(id_parent).find('.Mnk-tab_boton').filter('[data-contenedor="'+contenedor+'"]').removeClass('-active');
            $(id_parent).find('.Mnk-tab_content').filter('[data-contenedor="'+contenedor+'"]').removeClass('-active-desktop');
            $(this).addClass('-active');
            $('#'+id_tab).addClass('-active-desktop');
        }
    }else{
        $(id_parent).find('.Mnk-tab_boton').filter('[data-contenedor="'+contenedor+'"]').removeClass('-active');
        $(id_parent).find('.Mnk-tab_content').filter('[data-contenedor="'+contenedor+'"]').removeClass('-active-desktop');
        $(this).addClass('-active');
        $('#'+id_tab).addClass('-active-desktop');
    }
});
if($('.Mnk-tab_container').length){
    if(!$('.Mnk-tab_container').hasClass('Mnk-tab_only')){
        Mnk_toggle('.Mnk-tab_toggle-991','.Mnk-tab_container','-active-movil');
    };
};