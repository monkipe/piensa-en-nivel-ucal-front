(() => {

    'use strict';

    /**************** Gulp.js 4 configuration ****************/
    const

        // development or production
        devBuild = ((process.env.NODE_ENV || 'development').trim().toLowerCase() === 'development'),
        mode = require('gulp-mode')({
            modes: ['production', 'development'],
            default: 'production',
            verbose: false,
        }),

        // directory locations
        dir = {
            src: 'src/',
            build: 'build/'
        },

        // modules
        gulp = require('gulp'),
        del = require('del'),
        noop = require('gulp-noop'),
        newer = require('gulp-newer'),

        pug = require('gulp-pug'),
        data = require('gulp-data'),
        plumber = require('gulp-plumber'),
        notify = require('gulp-notify'),
        htmlmin = require('gulp-htmlmin'),

        size = require('gulp-size'),
        imagemin = require('gulp-imagemin'),
        sass = require('gulp-sass'), // or use gulp-dart-sass
        postcss = require('gulp-postcss'),
        combineMq = require('gulp-combine-mq'),
        compass = require('compass-importer'),
        sourcemaps = devBuild ? require('gulp-sourcemaps') : null,
        concatCss = require('gulp-concat-css'),
        browsersync = devBuild ? require('browser-sync').create() : null,
        concat = require('gulp-concat'),
        uglify = require('gulp-uglify-es').default,

        critical = require('critical').stream;

    var log = require('fancy-log');


    console.log('Gulp', devBuild ? 'development' : 'production', 'build');


    /**************** clean task ****************/

    function clean() {

        return del([dir.build]);

    }
    exports.clean = clean;
    exports.wipe = clean;


    /**************** images task ****************/

    const imgConfig = {
        src: dir.src + 'images/**/*',
        build: dir.build + 'images/',
        minOpts: {
            optimizationLevel: 5
        }
    };

    function images() {

        return gulp.src(imgConfig.src)
            .pipe(newer(imgConfig.build))
            .pipe(imagemin(imgConfig.minOpts))
            .pipe(size({ showFiles: true }))
            .pipe(gulp.dest(imgConfig.build));

    }
    exports.images = images;


    /**************** html task (private) ****************/

    const htmlConfig = {

        src: [
            dir.src + 'pug/**/*.pug',
            '!' + dir.src + 'pug/**/_*.pug'
        ],
        watch: dir.src + 'pug/**/*.pug',
        build: dir.build,
        htmlOpts: {
            //collapseWhitespace: true,
            minifyJS: true,
            //removeComments: true,
        },
        pugOpts: {
            basedir: 'src',
            pretty: true,
        }

    };

    function html() {
        return gulp.src(htmlConfig.src)
            .pipe(plumber({ errorHandler: notify.onError('Error: <%= error.message %>') }))
            .pipe(
                data(file => {
                    return {
                        relativePath: file.history[0].replace(file.base, ''),
                    }
                })
            )
            .pipe(pug(htmlConfig.pugOpts))
            .pipe(mode.production(htmlmin(htmlConfig.htmlOpts)))
            .pipe(gulp.dest(htmlConfig.build))
            .pipe(browsersync ? browsersync.reload({ stream: true }) : noop());

    }

    exports.html = html;


    /**************** CSS task ****************/

    const cssConfig = {

        src: [
            dir.src + 'scss/style.scss',
            dir.src + 'scss/responsive/responsive.scss'
        ],
        watch: dir.src + 'scss/**/*',
        build: dir.build + '',
        sassOpts: {
            sourceMap: devBuild,
            imagePath: '/images/',
            precision: 3,
            errLogToConsole: true,
            importer: compass
        },

        postCSS: [
            require('postcss-assets')({
                loadPaths: ['images/'],
                basePath: dir.build
            })
        ],
        postCSSProd: [
            require('postcss-uncss')({
                html: [dir.build + "*.html"],
                ignore: [
                    /Mnk-html-full/, /-complete/, /-open/, /Mnk-menu_body/, /-fixed/, /-active/, /-absolute/, /focus/, /focusx/,
                    /Mnk-modal-body/, /Mnk-modal-open/, /Mnk-modal-close/, /-active-desktop/, /-active-movil/,
                    /Mnk-tab_movil-oculto/, /Mnk-tab_movil-activo/, /-semicomplete/, /-slide-/,
                    /slick-list/, /slick-arrow/, /slick-next/, /slick-prev/, /slick-dots/,
                    /mfp-bg/, /mfp-title/, /mfp-arrow-left/, /mfp-arrow-right/, /mfp-zoom-in/, /mfp-iframe-scaler/, /mfp-zoom-out-cur/,
                    /cs-select/, /error/, /outdated/, /footer/, /-rotate-/, /-movil/, /-current/, /-parent-open/,
                    /Mnk-submenu_open/, /Mnk-menu_link/, /-desplegable/, /Mnk-page/,
                    /slick-negative/, /Mnk-modal_body/, /slick-slide/,/-ocult/
                ]
            }),
            require('postcss-assets')({
                loadPaths: ['images/'],
                basePath: dir.build
            }),
            require('autoprefixer')({
                overrideBrowserslist: ['last 4 version', 'safari 5', 'ie 7', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4']
            }),
            require('postcss-pxtorem')({
                replace: false
            }),
            require('cssnano')
        ]

    };


    function css() {

        return gulp.src(cssConfig.src)
            //.pipe(sourcemaps ? sourcemaps.init() : noop())
            .pipe(sass(cssConfig.sassOpts).on('error', sass.logError))
            .pipe(mode.production(postcss(cssConfig.postCSSProd)))
            .pipe(mode.development(postcss(cssConfig.postCSS)))
            //.pipe(postcss(cssConfig.postCSS))
            //.pipe(combineMq())
            //.pipe(sourcemaps ? sourcemaps.write() : noop())
            .pipe(size({ showFiles: true }))
            .pipe(gulp.dest(cssConfig.build))
            .pipe(browsersync ? browsersync.reload({ stream: true }) : noop());

    }

    function pluginCss() {

        return gulp.src(['node_modules/slick-carousel/slick/*.css', 'src/css/*.css'])
            .pipe(concatCss("plugin.css"))
            .pipe(gulp.dest(cssConfig.build));

    }

    function criticalCss() {
        return gulp.src([dir.build + "*.html"])
            .pipe(critical({
                base: 'dist/',
                inline: true,
                css: [dir.build + 'style.css', dir.build + 'plugin.css']
            }))
            .on('error', function (err) {
                log.error(err.message);
            })
            .pipe(gulp.dest('dist'));

    }

    exports.criticalCss = criticalCss;
    exports.concatCSS = pluginCss;

    exports.css = gulp.series(images, html, css);


    /**************** JS task ****************/

    const jsConfig = {

        src: [
            //dir.src + 'js/**/*'
            'node_modules/jquery-mask-plugin/dist/jquery.mask.min.js',
            'node_modules/jquery-validation/dist/jquery.validate.min.js',
            'node_modules/slick-carousel/slick/slick.min.js',
            dir.src + 'js/datepicker.min.js',
            dir.src + 'js/jquery.mousewheel.min.js',
            dir.src + 'js/index.js',
            dir.src + 'js/default.js',
            dir.src + 'js/modal.js',
            dir.src + 'js/tab.js',
            dir.src + 'js/svg.js',
        ],
        watch: dir.src + 'js/**/*.js',
        build: dir.build + 'js'

    };

    function scripts() {
        return gulp.src(jsConfig.src)
            .pipe(sourcemaps.init())
            .pipe(concat('mnk-query.js'))
            .pipe(uglify())
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(jsConfig.build)
            );
    }

    exports.js = gulp.series(scripts);


    /**************** server task (private) ****************/

    const syncConfig = {
        server: {
            baseDir: dir.build,
            index: 'index.html'
        },
        port: 8000,
        open: false
    };

    // browser-sync
    function server(done) {
        if (browsersync) browsersync.init(syncConfig);
        done();
    }


    /**************** watch task (private) ****************/

    function watch(done) {

        // image changes
        gulp.watch(imgConfig.src, images);

        // CSS changes
        gulp.watch(cssConfig.watch, css);

        // JS changes
        gulp.watch(jsConfig.watch, scripts);

        //gulp.watch(cssConfig.watch).on('change', browsersync.reload);
        // HTML changes
        gulp.watch(htmlConfig.watch, html);

        done();

    }

    /**************** default task ****************/

    exports.default = gulp.series(exports.css, exports.js, watch, server);

    exports.build = gulp.series(exports.css);



    var realFavicon = require('gulp-real-favicon');
    var fs = require('fs');

    // File where the favicon markups are stored
    var FAVICON_DATA_FILE = 'faviconData.json';

    // Generate the icons. This task takes a few seconds to complete.
    // You should run it at least once to create the icons. Then,
    // you should run it whenever RealFaviconGenerator updates its
    // package (see the check-for-favicon-update task below).
    gulp.task('generate-favicon', function (done) {
        realFavicon.generateFavicon({
            masterPicture: dir.src + 'images/favicon.png',
            dest: 'images/favicon',
            iconsPath: 'images/favicon',
            design: {
                ios: {
                    pictureAspect: 'backgroundAndMargin',
                    backgroundColor: '#ffffff',
                    margin: '28%',
                    assets: {
                        ios6AndPriorIcons: false,
                        ios7AndLaterIcons: false,
                        precomposedIcons: false,
                        declareOnlyDefaultIcon: true
                    }
                },
                desktopBrowser: {},
                windows: {
                    pictureAspect: 'noChange',
                    backgroundColor: '#e71f99',
                    onConflict: 'override',
                    assets: {
                        windows80Ie10Tile: false,
                        windows10Ie11EdgeTiles: {
                            small: false,
                            medium: true,
                            big: false,
                            rectangle: false
                        }
                    }
                },
                androidChrome: {
                    pictureAspect: 'noChange',
                    themeColor: '#e71f99',
                    manifest: {
                        name: 'Ucal',
                        display: 'standalone',
                        orientation: 'notSet',
                        onConflict: 'override',
                        declared: true
                    },
                    assets: {
                        legacyIcon: false,
                        lowResolutionIcons: false
                    }
                },
                safariPinnedTab: {
                    pictureAspect: 'silhouette',
                    themeColor: '#e71f99'
                }
            },
            settings: {
                scalingAlgorithm: 'Mitchell',
                errorOnImageTooSmall: false,
                readmeFile: false,
                htmlCodeFile: false,
                usePathAsIs: false
            },
            markupFile: FAVICON_DATA_FILE
        }, function () {
            done();
        });
    });

    // Inject the favicon markups in your HTML pages. You should run
    // this task whenever you modify a page. You can keep this task
    // as is or refactor your existing HTML pipeline.
    gulp.task('inject-favicon-markups', function () {
        return gulp.src([dir.build+"*html"])
            .pipe(realFavicon.injectFaviconMarkups(JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).favicon.html_code))
            .pipe(gulp.dest('dist'));
    });

    // Check for updates on RealFaviconGenerator (think: Apple has just
    // released a new Touch icon along with the latest version of iOS).
    // Run this task from time to time. Ideally, make it part of your
    // continuous integration system.
    gulp.task('check-for-favicon-update', function (done) {
        var currentVersion = JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).version;
        realFavicon.checkForUpdates(currentVersion, function (err) {
            if (err) {
                throw err;
            }
        });
    });



})();