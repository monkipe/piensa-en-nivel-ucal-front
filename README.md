# Información inicial de la web #

## Lineamientos generales

## Gulp

- El archivo que configura las tareas gulp se encuentra en la carpeta gulp-task / proyectos / pmrt.
- Verificar que la llamada del archivo de configuración de cada tarea gulp-task (sass, scripts, pug, etc.) sea del proyecto actual.

## Sass

- Actualizar las variables sass (colores, fuentes, tamaños) del proyecto actual sass / mixin / proyectos / pmrt

## Pug

- Actualizar las variables basicas del proyecto actual pug/ proyectos / pmrt
- Generar favicon del proyecto [https://realfavicongenerator.net/]


#### Jerarquia de clases

- Propias (header, footer, modal, etc.)
- Animation
- Position (relative, absolute, fixed, float, z-index)
- Container (table, block, inline-block)
- Width
- Height
- Margin
- Padding
- Border
- Background
- Content (align, font-size, line-height, font-weight, color)

## Docuemnto de la estructura

- https://drive.google.com/drive/folders/1XTuuHkpfDmANfdKep0Ew2lD4hvHqsb5-

## Colores
negro: #000000
blanco: #ffffff

gris-oscuro: #727272
gris-claro: #F1F6F9
rosa: #E90D8C
verde: #27A97C
rojo: #E41743
azul: #524FA1
amarillo: #CAD32B

## Fuentes
font-family: Rubik
font-weight: 400, 500, 700 
## Tamaños
Los tamaños cambiaran a partir de 451px y 992px

H1: 40 desktop - 26 tablet - 22 mobile / 500
H2: 36 desktop - 24 tablet - 18 mobile / 500
H3: 30 desktop - 18 tablet - 16 mobile / 500
H4: 24 desktop - 16 tablet - 15 mobile / 500
H5: 18 desktop - 15 tablet - 14 mobile / 500
H6: 16 desktop - 14 tablet - 13 mobile / 500

Texto: 16/22 desktop - 15 tablet - 15 mobile / 400 / color: #72727

Menú: 14-24 / 500 / color: #ffffff,0.6 / hover:#842A84
Botón: 14-30 / 700 / color: #ffffff / br:30px
Input: 14-17 / 500 / color: #727272 /
## Asignación de colores
Primario: Magenta: #D5178B
Secundario: verde: #04B39D